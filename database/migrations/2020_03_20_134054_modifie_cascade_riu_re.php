<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifieCascadeRiuRe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recette_ingredient_unites', function (Blueprint $table) {
            $table->dropForeign('recette_ingredient_unites_recette_foreign');
            $table->foreign('recette')->references('id')->on('recettes')->onDelete('cascade');
            $table->dropForeign('recette_ingredient_unites_ingredient_foreign');
            $table->foreign('ingredient')->references('id')->on('ingredients')->onDelete('cascade');
            $table->dropForeign('recette_ingredient_unites_unite_foreign');
            $table->foreign('unite')->references('id')->on('unites')->onDelete('cascade');
        });
        
        Schema::table('recette_etapes', function (Blueprint $table) {
            $table->dropForeign('recette_etapes_recette_foreign');
            $table->foreign('recette')->references('id')->on('recettes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
