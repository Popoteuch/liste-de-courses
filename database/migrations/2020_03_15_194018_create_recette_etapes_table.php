<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecetteEtapesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recette_etapes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('recette');
            $table->integer('etape');
            $table->string('description');
            $table->timestamps();
        });

        Schema::table('recette_etapes', function (Blueprint $table) {
            $table->foreign('recette')->references('id')->on('recettes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recette_etapes');
    }
}
