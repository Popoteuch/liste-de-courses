<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecetteIngredientUnitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recette_ingredient_unites', function (Blueprint $table) {
            $table->id();
            $table->integer('quantite');
            $table->unsignedBigInteger('recette');
            $table->unsignedBigInteger('ingredient');
            $table->unsignedBigInteger('unite');
            $table->timestamps();
        });

        Schema::table('recette_ingredient_unites', function (Blueprint $table) {
            $table->foreign('recette')->references('id')->on('recettes');
            $table->foreign('ingredient')->references('id')->on('ingredients');
            $table->foreign('unite')->references('id')->on('unites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recette_ingredient_unites');
    }
}
