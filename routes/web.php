<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('recette.index'));
});

Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::resource('ingredient', 'IngredientController');
    Route::resource('unite', 'UniteController');
    Route::resource('panier', 'PanierController');
    Route::resource('user', 'UserController');
    Route::get('panier/{recette}-{personnes}', ['as' => 'panier.ajoute/{recette}-{personnes}', 'uses' => 'PanierController@ajouteRecette'])->where('recette', '[0-9]+')->where('personnes', '[0-9]+');
    Route::post('panier/{recette}', ['as' => 'panier.store/{recette}', 'uses' => 'PanierController@store']);
    Route::delete('panier', ['as' => 'panier.destroyAll', 'uses' => 'PanierController@destroyAll']);
    Route::get('liste-de-courses', ['as' => 'panier.liste', 'uses' => 'PanierController@liste']);
    Route::post('ingredient/recherche', ['as' => 'ingredient.recherche', 'uses' => 'IngredientController@recherche']);
    Route::post('unite/recherche', ['as' => 'unite.recherche', 'uses' => 'UniteController@recherche']);
    Route::post('user/recherche', ['as' => 'user.recherche', 'uses' => 'UserController@recherche']);
});

// Route::resource('user', 'UserController');
Route::resource('recette', 'RecetteController');
Route::resource('recette_etape', 'RecetteEtapeController');
Route::resource('recette_ingredient_unite', 'RecetteIngredientUniteController');
Route::post('recette/recherche', ['as' => 'recette.recherche', 'uses' => 'RecetteController@recherche']);
Route::get('recette_etape/{recette}/create', ['as' => 'recette_etape.create/{recette}', 'uses' => 'RecetteEtapeController@create']);
Route::get('recette_ingredient_unite/{recette}/create', ['as' => 'recette_ingredient_unite.create/{recette}', 'uses' => 'RecetteIngredientUniteController@create']);

Route::get('deconnexion', ['as' => 'deconnexion', function() {
    Auth::logout();
}]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('image-upload', 'ImageUploadController@imageUpload')->name('image.upload');
Route::post('image-upload', 'ImageUploadController@imageUploadPost')->name('image.upload.post');
