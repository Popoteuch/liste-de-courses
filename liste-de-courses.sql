-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour liste-de-courses
DROP DATABASE IF EXISTS `liste-de-courses`;
CREATE DATABASE IF NOT EXISTS `liste-de-courses` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `liste-de-courses`;

-- Listage de la structure de la table liste-de-courses. failed_jobs
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.failed_jobs : ~0 rows (environ)
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. images
DROP TABLE IF EXISTS `images`;
CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.images : ~28 rows (environ)
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
INSERT INTO `images` (`id`, `nom`, `created_at`, `updated_at`) VALUES
	(1, '1584307220.png', '2020-03-15 21:20:20', '2020-03-15 21:20:20'),
	(2, '1584308115.png', '2020-03-15 21:35:15', '2020-03-15 21:35:15'),
	(3, '1584308183.png', '2020-03-15 21:36:23', '2020-03-15 21:36:23'),
	(4, '1584308620.png', '2020-03-15 21:43:40', '2020-03-15 21:43:40'),
	(5, '1584309349.png', '2020-03-15 21:55:49', '2020-03-15 21:55:49'),
	(6, '1584309362.png', '2020-03-15 21:56:02', '2020-03-15 21:56:02'),
	(7, '1584309375.png', '2020-03-15 21:56:15', '2020-03-15 21:56:15'),
	(8, '1584309554.png', '2020-03-15 21:59:14', '2020-03-15 21:59:14'),
	(9, '1584309973.png', '2020-03-15 22:06:13', '2020-03-15 22:06:13'),
	(10, '1584309985.png', '2020-03-15 22:06:25', '2020-03-15 22:06:25'),
	(11, '1584375959.jpg', '2020-03-16 16:25:59', '2020-03-16 16:25:59'),
	(12, '1584708844.jpg', '2020-03-20 12:54:04', '2020-03-20 12:54:04'),
	(13, '1584798115.jpg', '2020-03-21 13:41:55', '2020-03-21 13:41:55'),
	(14, '1584809829.jpg', '2020-03-21 16:57:09', '2020-03-21 16:57:09'),
	(15, '1584809847.jpg', '2020-03-21 16:57:27', '2020-03-21 16:57:27'),
	(16, '1584809860.jpg', '2020-03-21 16:57:40', '2020-03-21 16:57:40'),
	(17, '1585220384.jpg', '2020-03-26 10:59:44', '2020-03-26 10:59:44'),
	(18, '1585222619.jpg', '2020-03-26 11:36:59', '2020-03-26 11:36:59'),
	(19, '1585223461.jpg', '2020-03-26 11:51:01', '2020-03-26 11:51:01'),
	(20, '1585223587.jpg', '2020-03-26 11:53:07', '2020-03-26 11:53:07'),
	(21, '1585229364.jpg', '2020-03-26 13:29:24', '2020-03-26 13:29:24'),
	(22, '1585229372.jpg', '2020-03-26 13:29:32', '2020-03-26 13:29:32'),
	(23, '1585229391.jpg', '2020-03-26 13:29:51', '2020-03-26 13:29:51'),
	(24, '1585229400.jpg', '2020-03-26 13:30:00', '2020-03-26 13:30:00'),
	(25, '1585230167.jpg', '2020-03-26 13:42:47', '2020-03-26 13:42:47'),
	(26, '1585230174.jpg', '2020-03-26 13:42:54', '2020-03-26 13:42:54'),
	(27, '1585230295.jpeg', '2020-03-26 13:44:55', '2020-03-26 13:44:55'),
	(28, '1585231073.jpg', '2020-03-26 13:57:53', '2020-03-26 13:57:53'),
	(29, '1585238347.jpg', '2020-03-26 15:59:07', '2020-03-26 15:59:07');
/*!40000 ALTER TABLE `images` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. ingredients
DROP TABLE IF EXISTS `ingredients`;
CREATE TABLE IF NOT EXISTS `ingredients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.ingredients : ~54 rows (environ)
/*!40000 ALTER TABLE `ingredients` DISABLE KEYS */;
INSERT INTO `ingredients` (`id`, `nom`, `created_at`, `updated_at`) VALUES
	(1, 'Banane', '2020-03-14 14:50:56', '2020-03-15 15:50:09'),
	(2, 'Patate', '2020-03-14 14:59:59', '2020-03-14 14:59:59'),
	(3, 'Carotte', '2020-03-14 15:01:01', '2020-03-14 15:01:01'),
	(4, 'Tomate', '2020-03-14 15:01:28', '2020-03-14 15:01:28'),
	(5, 'Pomme', '2020-03-14 15:03:06', '2020-03-14 15:03:06'),
	(6, 'Fromage', '2020-03-14 15:25:13', '2020-03-14 15:25:13'),
	(7, 'Betterave', '2020-03-14 15:25:33', '2020-03-14 15:25:33'),
	(8, 'Poulet', '2020-03-14 15:26:01', '2020-03-14 15:26:01'),
	(10, 'Courgette', '2020-03-14 16:15:22', '2020-03-14 16:15:22'),
	(12, 'Asperge', '2020-03-14 18:01:32', '2020-03-14 18:01:32'),
	(13, 'Eau', '2020-03-15 15:50:18', '2020-03-15 15:50:18'),
	(15, 'Polenta', '2020-03-20 12:57:27', '2020-03-20 12:57:27'),
	(16, 'Lait', '2020-03-20 12:57:33', '2020-03-20 12:57:33'),
	(17, 'Cumin en poudre', '2020-03-20 12:57:42', '2020-03-20 12:57:42'),
	(18, 'Munster Ermitage', '2020-03-20 12:57:51', '2020-03-20 12:57:51'),
	(19, 'Poire', '2020-03-20 12:57:58', '2020-03-20 12:57:58'),
	(20, 'Cumin', '2020-03-20 12:58:12', '2020-03-20 12:58:12'),
	(21, 'Huile d\'olive', '2020-03-20 12:58:23', '2020-03-20 12:58:23'),
	(22, 'Sel', '2020-03-20 12:58:34', '2020-03-20 12:58:34'),
	(23, 'Poivre', '2020-03-20 12:58:38', '2020-03-20 12:58:38'),
	(24, 'Farine', '2020-03-21 13:42:57', '2020-03-21 13:42:57'),
	(25, 'Levure de boulanger', '2020-03-21 13:43:15', '2020-03-21 13:43:15'),
	(26, 'Sucre en poudre', '2020-03-21 13:43:29', '2020-03-21 13:43:29'),
	(27, 'Minibri Ermitage', '2020-03-21 13:43:49', '2020-03-21 13:43:49'),
	(28, 'Viande hachée de boeuf', '2020-03-21 13:43:56', '2020-03-21 13:43:56'),
	(29, 'Oignon', '2020-03-21 13:45:53', '2020-03-21 13:45:53'),
	(30, 'Coulis de tomate', '2020-03-21 13:46:02', '2020-03-21 13:46:02'),
	(31, 'Gousse d\'ail', '2020-03-21 13:46:11', '2020-03-21 13:46:11'),
	(32, 'Persil haché', '2020-03-21 13:46:19', '2020-03-21 13:46:19'),
	(33, 'Paprika', '2020-03-21 13:46:26', '2020-03-21 13:46:26'),
	(34, 'Origan séché', '2020-03-21 13:46:34', '2020-03-21 13:46:34'),
	(35, 'Chasseur Dual Blades', '2020-03-21 16:58:12', '2020-03-26 10:56:01'),
	(36, 'Pâte brisée', '2020-03-26 11:00:42', '2020-03-26 11:00:42'),
	(37, 'Patate douce', '2020-03-26 11:00:47', '2020-03-26 11:00:47'),
	(38, 'Fromage râpé', '2020-03-26 11:01:12', '2020-03-26 11:01:12'),
	(39, 'Curry en poudre', '2020-03-26 11:01:28', '2020-03-26 11:01:28'),
	(40, 'Jaune d\'oeuf', '2020-03-26 11:01:39', '2020-03-26 11:05:07'),
	(41, 'Jeune pousse', '2020-03-26 11:01:55', '2020-03-26 11:05:12'),
	(42, 'Vinaigrette', '2020-03-26 11:02:16', '2020-03-26 11:02:16'),
	(43, 'Céréales et lentilles Ebly', '2020-03-26 11:37:25', '2020-03-26 11:37:25'),
	(44, 'Coriandre ciselée', '2020-03-26 11:37:44', '2020-03-26 11:37:44'),
	(45, 'Oeuf', '2020-03-26 11:38:50', '2020-03-26 11:38:50'),
	(46, 'Pain burger', '2020-03-26 11:41:28', '2020-03-26 11:41:28'),
	(47, 'Salade verte', '2020-03-26 11:41:37', '2020-03-26 11:41:37'),
	(48, 'Moutarde', '2020-03-26 11:41:45', '2020-03-26 11:41:45'),
	(49, 'Ketchup', '2020-03-26 11:41:53', '2020-03-26 11:41:53'),
	(50, 'Salade roquette', '2020-03-26 11:53:34', '2020-03-26 11:53:34'),
	(51, 'Thym', '2020-03-26 11:53:42', '2020-03-26 11:53:42'),
	(52, 'Vinaigre', '2020-03-26 11:54:24', '2020-03-26 11:54:24'),
	(53, 'Persil', '2020-03-26 13:51:21', '2020-03-26 13:51:21'),
	(54, 'Vin blanc', '2020-03-26 13:51:43', '2020-03-26 13:51:43'),
	(55, 'Échalote', '2020-03-26 13:51:55', '2020-03-26 13:51:55'),
	(56, 'Beurre', '2020-03-26 13:52:02', '2020-03-26 13:52:02'),
	(57, 'Moule', '2020-03-26 13:52:10', '2020-03-26 13:52:10'),
	(59, 'Lait de coco', '2020-03-26 15:10:16', '2020-03-26 15:10:16'),
	(60, 'Tabasco', '2020-03-26 15:10:33', '2020-03-26 15:10:33');
/*!40000 ALTER TABLE `ingredients` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. migrations
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.migrations : ~24 rows (environ)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(6, '2020_03_12_201012_create_posts_table', 1),
	(14, '2020_03_12_204326_create_ingredients_table', 2),
	(18, '2014_10_12_000000_create_users_table', 3),
	(19, '2019_08_19_000000_create_failed_jobs_table', 3),
	(20, '2020_03_12_201816_create_posts_table', 3),
	(21, '2020_03_12_210906_create_ingredients_table', 3),
	(22, '2020_03_14_195859_create_type_ingredients_table', 4),
	(24, '2020_03_14_205005_create_unites_table', 5),
	(25, '2014_10_12_100000_create_password_resets_table', 6),
	(31, '2020_03_15_192830_create_recettes_table', 7),
	(32, '2020_03_15_193348_create_recette_ingredient_unites_table', 7),
	(33, '2020_03_15_194018_create_recette_etapes_table', 7),
	(34, '2020_03_15_194558_add_description_recette_table', 8),
	(35, '2020_03_15_205637_create_images_table', 9),
	(36, '2020_03_15_213728_add_image_recette_table', 10),
	(37, '2020_03_15_213943_add_image_recette_table', 11),
	(39, '2020_03_20_130606_modifie_type_quantite_table_riu', 12),
	(40, '2020_03_20_131238_modifie_type_quantite2_table_riu', 13),
	(42, '2020_03_20_134054_modifie_cascade_riu_re', 14),
	(43, '2020_03_20_142008_add_personnes_recette', 15),
	(44, '2020_03_20_143137_create_panier_table', 16),
	(45, '2020_03_20_143519_create_paniers_table', 17),
	(46, '2020_03_26_142101_add_admin_user_table', 17);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. paniers
DROP TABLE IF EXISTS `paniers`;
CREATE TABLE IF NOT EXISTS `paniers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user` bigint(20) unsigned NOT NULL,
  `recette` bigint(20) unsigned NOT NULL,
  `personnes` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `paniers_user_foreign` (`user`),
  KEY `paniers_recette_foreign` (`recette`),
  CONSTRAINT `paniers_recette_foreign` FOREIGN KEY (`recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `paniers_user_foreign` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.paniers : ~0 rows (environ)
/*!40000 ALTER TABLE `paniers` DISABLE KEYS */;
/*!40000 ALTER TABLE `paniers` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. password_resets
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.password_resets : ~0 rows (environ)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
	('admin@admin.com', '$2y$10$IbVcoR/XfRiYYCNiT3.nv.4DGySh0IVkNMAA/1gBLaAana2QLnsk2', '2020-03-15 16:15:45');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. recettes
DROP TABLE IF EXISTS `recettes`;
CREATE TABLE IF NOT EXISTS `recettes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `temps` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` bigint(20) unsigned NOT NULL,
  `personnes` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `recettes_image_foreign` (`image`),
  CONSTRAINT `recettes_image_foreign` FOREIGN KEY (`image`) REFERENCES `images` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.recettes : ~8 rows (environ)
/*!40000 ALTER TABLE `recettes` DISABLE KEYS */;
INSERT INTO `recettes` (`id`, `nom`, `temps`, `created_at`, `updated_at`, `description`, `image`, `personnes`) VALUES
	(9, 'Toast au polenta poire et Munster', '30 min', '2020-03-20 12:54:04', '2020-03-20 12:54:04', 'Des bouchées originales et faciles à préparer. A servir en apéritif, pour un buffet dînatoire ou entrée', 12, 5),
	(10, 'Pizza pide (pizza turque) au "Minibri"', '40 min', '2020-03-21 13:41:55', '2020-03-21 13:41:55', 'Une pizza originale et savoureuse à préparer maison !', 13, 4),
	(12, 'Safi\'Jiva et sa dose de sel', 'environ 45min', '2020-03-21 16:57:40', '2020-03-26 11:44:12', 'Repas plutôt laborieux à faire mais c\'est pas grave vu la douceur du plat', 16, 4),
	(13, 'Chaussons de patate douce', '20 min', '2020-03-26 10:59:44', '2020-03-26 10:59:44', 'Nous avons dévoré ces petits chaussons moelleux à la patate douce. Ca change de la quiche, non ?', 17, 4),
	(14, 'Burger végétarien aux céréales et lentilles', '30 min', '2020-03-26 11:36:59', '2020-03-26 11:36:59', 'Un burger végétarien gourmand et simple à préparer à la maison !', 18, 4),
	(16, 'Poulet fermier rôti au four', '10 min', '2020-03-26 11:53:07', '2020-03-26 13:42:54', 'Le plat préféré des français ! Le plaisir des dimanches en famille... Quelques astuces pour réussir un rôti.', 26, 4),
	(17, 'Moules marinières à la cocotte faciles', '30 min', '2020-03-26 13:44:55', '2020-03-26 13:44:55', 'Un classique de la cuisine des bords de mer.', 27, 4),
	(18, 'Poulet, curry et coco', '10 min', '2020-03-26 13:57:53', '2020-03-26 13:57:53', 'Un plat en sauce, très simple à réaliser, peu se faire avec viande ou poisson, et en plus fait voyager', 28, 4);
/*!40000 ALTER TABLE `recettes` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. recette_etapes
DROP TABLE IF EXISTS `recette_etapes`;
CREATE TABLE IF NOT EXISTS `recette_etapes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `recette` bigint(20) unsigned NOT NULL,
  `etape` int(11) NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recette_etapes_recette_foreign` (`recette`),
  CONSTRAINT `recette_etapes_recette_foreign` FOREIGN KEY (`recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.recette_etapes : ~50 rows (environ)
/*!40000 ALTER TABLE `recette_etapes` DISABLE KEYS */;
INSERT INTO `recette_etapes` (`id`, `recette`, `etape`, `description`, `created_at`, `updated_at`) VALUES
	(5, 9, 1, 'Portez le lait salé à ébullition avec 1 cuillère à soupe d\'huile d\'olive et le cumin en poudre, puis versez la polenta en pluie.', '2020-03-20 13:35:26', '2020-03-20 13:35:26'),
	(6, 9, 2, 'Laissez cuire 2 à 3 minutes sans cesser de remuer, jusqu\'à ce que la polenta se détache des parois de la casserole. Hors du feu, ajoutez le poivre et mélangez.', '2020-03-20 13:36:25', '2020-03-20 13:36:25'),
	(7, 9, 3, 'Étalez la polenta dans un plat huilé à l\'huile d\'olive sur 2 cm d\'épaisseur et laissez complètement refroidir.', '2020-03-20 13:36:36', '2020-03-20 13:36:36'),
	(8, 9, 4, 'Épluchez les poires et coupez-les en lamelles.', '2020-03-20 13:36:45', '2020-03-20 13:36:45'),
	(9, 9, 5, 'Coupez le Munster en tranches.', '2020-03-20 13:36:54', '2020-03-20 13:36:54'),
	(10, 9, 6, 'Découpez la polenta en toasts carrés. Déposez-les sur une plaque recouverte de papier sulfurisé. Déposez une lamelle de poire sur chaque toast de polenta.', '2020-03-20 13:37:09', '2020-03-20 13:37:09'),
	(11, 9, 7, 'Mettez par dessus une tranche ou deux de Munster.', '2020-03-20 13:37:18', '2020-03-20 13:37:18'),
	(12, 9, 8, 'Saupoudrez de graines de cumin et d\'un très léger filet d\'huile d\'olive.\r\nMettez la plaque dans un four préchauffé à 200°C, thermostat 7, pendant 10 minutes.', '2020-03-20 13:37:36', '2020-03-20 13:37:36'),
	(13, 9, 9, 'Dégustez.', '2020-03-20 13:38:39', '2020-03-20 13:38:39'),
	(14, 10, 1, 'Dans la cuve de votre robot ou dans un saladier, versez la farine, l’eau, la levure boulangère, le sel et le sucre.', '2020-03-21 13:50:48', '2020-03-21 13:50:48'),
	(15, 10, 2, 'Mélangez pendant 5 minutes à vitesse lente, puis 8 minutes à vitesse rapide. Environ 2 minutes avant la fin, incorporez l’huile d’olive.', '2020-03-21 13:51:01', '2020-03-21 13:51:01'),
	(16, 10, 3, 'Formez une boule, recouvrez-la d’un linge humide. Laissez pousser 2 heures à température ambiante.', '2020-03-21 13:51:11', '2020-03-21 13:51:11'),
	(17, 10, 4, 'Dans une poêle, faites revenir 2 minutes l\'oignon et l\'ail hachés avec l\'huile d\'olive.\r\nAjoutez le coulis de tomate, le paprika et l\'origan puis laissez mijoter 2 minutes.', '2020-03-21 13:51:20', '2020-03-21 13:51:20'),
	(18, 10, 5, 'Ajoutez la viande hachée, le persil, le sel et le poivre et laissez mijoter 5 minutes.', '2020-03-21 13:51:29', '2020-03-21 13:51:29'),
	(19, 10, 6, 'Coupez le Minibri en dés.', '2020-03-21 13:51:40', '2020-03-21 13:51:40'),
	(20, 10, 7, 'Farinez le plan de travail. Dégazez la pâte en la pétrissant un peu puis étalez-la à la main sur une plaque recouverte de papier sulfurisé. Formez un joli ovale pour obtenir une pizza pide.', '2020-03-21 13:51:52', '2020-03-21 13:51:52'),
	(21, 10, 8, 'Disposez la garniture de viande au milieu de la pâte à pizza en laissant de l’espace sur les côtés.', '2020-03-21 13:52:01', '2020-03-21 13:52:01'),
	(22, 10, 9, 'Saupoudrez la garniture de dés de Minibri.', '2020-03-21 13:52:11', '2020-03-21 13:52:11'),
	(23, 10, 10, 'Repliez les côtés en les roulant légèrement afin de former une pizza ovale et allongée.\r\nBadigeonnez les bords d’huile d’olive.\r\nMettez à cuire 20 minutes dans un four préchauffé à 220°C.', '2020-03-21 13:52:19', '2020-03-21 13:52:19'),
	(24, 10, 11, 'Dégustez.', '2020-03-21 13:52:29', '2020-03-21 13:52:29'),
	(25, 12, 1, 'Le tuer', '2020-03-21 16:58:44', '2020-03-21 16:58:44'),
	(26, 12, 2, 'Le loot', '2020-03-21 16:58:55', '2020-03-21 16:58:55'),
	(27, 12, 3, 'Bien prendre les doubles lames', '2020-03-21 16:59:06', '2020-03-26 11:44:23'),
	(28, 12, 4, 'Le manger', '2020-03-21 16:59:15', '2020-03-21 16:59:15'),
	(30, 13, 1, 'Pelez puis coupez patate douce et pommes de terre en dés et faites-les cuire à la vapeur.', '2020-03-26 11:19:09', '2020-03-26 11:19:09'),
	(31, 13, 2, 'Coupez le fromage en dés.', '2020-03-26 11:19:17', '2020-03-26 11:19:17'),
	(32, 13, 3, 'Ajoutez le fromage aux légumes cuits et écrasez à la fourchette.', '2020-03-26 11:19:23', '2020-03-26 11:19:23'),
	(33, 13, 4, 'Ajoutez l\'huile.', '2020-03-26 11:19:30', '2020-03-26 11:19:30'),
	(34, 13, 5, 'Ajoutez les épices et le sel et mélangez.', '2020-03-26 11:19:39', '2020-03-26 11:19:39'),
	(35, 13, 6, 'Coupez 16 cercles de 9 cm de diamètre dans les pâtes.', '2020-03-26 11:19:45', '2020-03-26 11:19:45'),
	(36, 13, 7, 'Ajoutez une cuillère à soupe de purée au centre de chaque cercle.', '2020-03-26 11:19:52', '2020-03-26 11:19:52'),
	(37, 13, 8, 'Refermez les cercles en demi-lune.', '2020-03-26 11:19:59', '2020-03-26 11:19:59'),
	(38, 13, 9, 'Appuyez bien sur tout le pourtour afin de sceller les bords.', '2020-03-26 11:20:10', '2020-03-26 11:20:10'),
	(39, 13, 10, 'Prenez chaque chausson dans la main puis roulez les bords afin de fermer hermétiquement chaque chausson.', '2020-03-26 11:20:18', '2020-03-26 11:20:18'),
	(40, 13, 11, 'Badigeonnez les chaussons avec le mélange oeuf et lait.', '2020-03-26 11:20:27', '2020-03-26 11:20:27'),
	(41, 13, 12, 'Préchauffez le four à 180°C ou Th.6. Enfournez pour une quinzaine de minutes. Les empanadas doivent être bien dorés. Laissez tiédir sur une grille et servez.', '2020-03-26 11:20:33', '2020-03-26 11:20:33'),
	(42, 14, 1, 'Disposez tous les ingrédients sur un plateau.', '2020-03-26 11:45:43', '2020-03-26 11:45:43'),
	(43, 14, 2, 'Faites cuire les Céréales et Lentilles selon le mode d\'emploi. Une fois cuits, ôtez le sachet et versez les Céréales et Lentilles dans un saladier.', '2020-03-26 11:45:51', '2020-03-26 11:45:51'),
	(44, 14, 3, 'Écrasez la patate douce avec une fourchette. Coupez l\'oignon en dés et faites-le revenir 5 minutes avec un filet d\'huile d\'olive, l\'ail haché et le cumin. Faites refroidir.', '2020-03-26 11:46:00', '2020-03-26 11:46:00'),
	(45, 14, 4, 'Dans un saladier, ajoutez la purée de patate douce et l\'oignon saisi avec le mélange de Céréales et Lentilles.', '2020-03-26 11:46:06', '2020-03-26 11:46:06'),
	(46, 14, 5, 'Ajoutez la coriandre, la farine, le sel, le poivre et l\'œuf.', '2020-03-26 11:46:11', '2020-03-26 11:46:11'),
	(47, 14, 6, 'Mélangez bien le tout.', '2020-03-26 11:46:16', '2020-03-26 11:46:16'),
	(48, 14, 7, 'Formez des galettes de la taille du pain, disposez-les sur une plaque et mettez-les au frais pendant 30 minutes.', '2020-03-26 11:46:23', '2020-03-26 11:46:23'),
	(49, 14, 8, 'Chauffez une poêle avec 3 cuillères à soupe d’huile d’olive et faites dorer les galettes des deux faces.', '2020-03-26 11:46:31', '2020-03-26 11:46:31'),
	(50, 14, 9, 'Badigeonnez les pains burger de moutarde, déposez une feuille de salade et une rondelle de tomate.', '2020-03-26 11:46:35', '2020-03-26 11:46:35'),
	(51, 14, 10, 'Ajoutez une galette sur le dessus.', '2020-03-26 11:46:42', '2020-03-26 11:46:42'),
	(52, 14, 11, 'Terminez avec une autre rondelle de tomate et un peu de ketchup. Fermez le burger.', '2020-03-26 11:46:50', '2020-03-26 11:46:50'),
	(53, 14, 12, 'Bonne dégustation !', '2020-03-26 11:46:57', '2020-03-26 11:46:57'),
	(54, 16, 1, 'Assaisonner l\'intérieur du poulet : sel, poivre, thym et gousses d\'ail en chemise.\r\nSaler légèrement l\'extérieur du poulet.\r\nColorer le poulet sur toutes ses faces dans l\'huile fumante.', '2020-03-26 11:56:26', '2020-03-26 11:56:26'),
	(55, 16, 2, 'Enfourner le poulet dans un four chaud à 180°C ou Th.6..\r\nRetourner la pièce pendant la cuisson et arroser de temps en temps.\r\nVérifier la cuisson du poulet en piquant les cuisses. Le jus qui s\'écoule doit être translucide et blanc.', '2020-03-26 11:56:35', '2020-03-26 11:56:35'),
	(56, 16, 3, 'Sortir le poulet du four et le mettre dans un endroit tiède.\r\nRéaliser le jus de rôti : pincer les sucs de cuisson, dégraisser et déglacer à l\'eau. Rectifier l\'assaisonnement et ajouter quelques gouttes de vinaigre de vin rouge.', '2020-03-26 11:56:40', '2020-03-26 11:56:40'),
	(57, 16, 4, 'Découper le poulet et servir avec le jus bouillant.\r\nServir avec une salade roquette !', '2020-03-26 11:56:46', '2020-03-26 11:56:46'),
	(59, 17, 1, 'Nettoyez les moules et rincez-les à l\'eau froide. Coupez les échalotes en dés. \r\nDans une cocotte, placez-y le beurre, les échalotes et le persil haché.\r\nAjoutez les moules et mouillez-les avec le vin blanc. Salez, poivrez.', '2020-03-26 13:56:34', '2020-03-26 13:56:34'),
	(60, 17, 2, 'Faites cuire à feu vif, avec le couvercle, une dizaine de minutes à peine.\r\nUne fois que les coques sont ouvertes, les moules sont cuites. Dressez les moules dans un plat, et arrosez-les de jus de cuisson et de persil.', '2020-03-26 13:56:45', '2020-03-26 13:56:45'),
	(61, 18, 1, 'Faire revenir le poulet en petits morceaux environ 5 minutes, réserver.', '2020-03-26 15:12:16', '2020-03-26 15:12:16'),
	(62, 18, 2, 'Dans le reste d\'huile, faire revenir les échalotes ciselées.\r\nAjouter farine et poudre de curry, cuire 1 minute.', '2020-03-26 15:12:21', '2020-03-26 15:12:21'),
	(63, 18, 3, 'Ajouter petit à petit lait de coco en remuant, faire bouillir.', '2020-03-26 15:12:26', '2020-03-26 15:12:26'),
	(64, 18, 4, 'Remettre le poulet, ajouter piment et sel selon le goût.\r\nMettre à feu doux/moyen pendant 20 minutes environ.', '2020-03-26 15:12:31', '2020-03-26 15:12:31');
/*!40000 ALTER TABLE `recette_etapes` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. recette_ingredient_unites
DROP TABLE IF EXISTS `recette_ingredient_unites`;
CREATE TABLE IF NOT EXISTS `recette_ingredient_unites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `quantite` decimal(10,2) NOT NULL,
  `recette` bigint(20) unsigned NOT NULL,
  `ingredient` bigint(20) unsigned NOT NULL,
  `unite` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recette_ingredient_unites_recette_foreign` (`recette`),
  KEY `recette_ingredient_unites_ingredient_foreign` (`ingredient`),
  KEY `recette_ingredient_unites_unite_foreign` (`unite`),
  CONSTRAINT `recette_ingredient_unites_ingredient_foreign` FOREIGN KEY (`ingredient`) REFERENCES `ingredients` (`id`) ON DELETE CASCADE,
  CONSTRAINT `recette_ingredient_unites_recette_foreign` FOREIGN KEY (`recette`) REFERENCES `recettes` (`id`) ON DELETE CASCADE,
  CONSTRAINT `recette_ingredient_unites_unite_foreign` FOREIGN KEY (`unite`) REFERENCES `unites` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.recette_ingredient_unites : ~66 rows (environ)
/*!40000 ALTER TABLE `recette_ingredient_unites` DISABLE KEYS */;
INSERT INTO `recette_ingredient_unites` (`id`, `quantite`, `recette`, `ingredient`, `unite`, `created_at`, `updated_at`) VALUES
	(3, 120.00, 9, 15, 11, '2020-03-20 12:59:38', '2020-03-20 12:59:38'),
	(4, 60.00, 9, 16, 3, '2020-03-20 12:59:49', '2020-03-20 12:59:49'),
	(5, 0.20, 9, 17, 10, '2020-03-20 13:03:52', '2020-03-20 13:16:48'),
	(7, 150.00, 9, 18, 11, '2020-03-20 13:25:51', '2020-03-20 13:25:51'),
	(8, 2.00, 9, 19, 13, '2020-03-20 13:26:06', '2020-03-20 13:26:06'),
	(9, 0.20, 9, 20, 10, '2020-03-20 13:26:35', '2020-03-20 13:26:35'),
	(10, 3.00, 9, 21, 9, '2020-03-20 13:26:47', '2020-03-20 13:26:47'),
	(11, 1.00, 9, 22, 14, '2020-03-20 13:27:00', '2020-03-20 13:27:00'),
	(12, 1.00, 9, 23, 14, '2020-03-20 13:27:07', '2020-03-20 13:27:07'),
	(13, 250.00, 10, 24, 11, '2020-03-21 13:47:07', '2020-03-21 13:47:07'),
	(14, 15.00, 10, 13, 3, '2020-03-21 13:47:20', '2020-03-21 13:47:20'),
	(15, 10.00, 10, 25, 11, '2020-03-21 13:47:33', '2020-03-21 13:47:33'),
	(16, 0.50, 10, 22, 10, '2020-03-21 13:47:52', '2020-03-21 13:47:52'),
	(17, 1.00, 10, 26, 10, '2020-03-21 13:48:07', '2020-03-21 13:48:07'),
	(18, 5.00, 10, 21, 9, '2020-03-21 13:48:18', '2020-03-21 13:53:23'),
	(19, 1.00, 10, 27, 13, '2020-03-21 13:48:33', '2020-03-21 13:48:33'),
	(20, 200.00, 10, 28, 11, '2020-03-21 13:48:46', '2020-03-21 13:48:46'),
	(21, 1.00, 10, 29, 13, '2020-03-21 13:48:56', '2020-03-21 13:48:56'),
	(22, 10.00, 10, 30, 3, '2020-03-21 13:49:10', '2020-03-21 13:49:10'),
	(23, 1.00, 10, 31, 13, '2020-03-21 13:49:20', '2020-03-21 13:49:20'),
	(24, 1.00, 10, 32, 9, '2020-03-21 13:49:31', '2020-03-21 13:49:31'),
	(25, 1.00, 10, 33, 10, '2020-03-21 13:49:42', '2020-03-21 13:49:42'),
	(27, 0.50, 10, 34, 10, '2020-03-21 13:50:12', '2020-03-21 13:50:12'),
	(28, 1.00, 10, 22, 14, '2020-03-21 13:50:23', '2020-03-21 13:50:23'),
	(29, 1.00, 10, 23, 14, '2020-03-21 13:50:33', '2020-03-21 13:50:33'),
	(30, 4.00, 12, 35, 13, '2020-03-21 16:58:34', '2020-03-21 16:58:34'),
	(31, 1.00, 13, 36, 15, '2020-03-26 11:02:41', '2020-03-26 11:02:41'),
	(32, 250.00, 13, 37, 11, '2020-03-26 11:02:55', '2020-03-26 11:02:55'),
	(33, 100.00, 13, 2, 11, '2020-03-26 11:03:05', '2020-03-26 11:03:05'),
	(34, 100.00, 13, 38, 11, '2020-03-26 11:03:18', '2020-03-26 11:03:18'),
	(35, 1.00, 13, 21, 9, '2020-03-26 11:03:28', '2020-03-26 11:03:28'),
	(36, 1.00, 13, 39, 10, '2020-03-26 11:03:50', '2020-03-26 11:03:50'),
	(37, 1.00, 13, 16, 9, '2020-03-26 11:04:01', '2020-03-26 11:04:01'),
	(38, 1.00, 13, 40, 13, '2020-03-26 11:04:32', '2020-03-26 11:04:32'),
	(39, 1.00, 13, 22, 14, '2020-03-26 11:04:41', '2020-03-26 11:04:41'),
	(40, 1.00, 13, 42, 13, '2020-03-26 11:04:52', '2020-03-26 11:04:52'),
	(41, 1.00, 13, 41, 16, '2020-03-26 11:05:01', '2020-03-26 11:05:01'),
	(42, 2.00, 14, 43, 16, '2020-03-26 11:38:05', '2020-03-26 11:38:05'),
	(43, 1.00, 14, 37, 13, '2020-03-26 11:38:16', '2020-03-26 11:38:16'),
	(44, 1.00, 14, 29, 13, '2020-03-26 11:38:23', '2020-03-26 11:38:23'),
	(45, 1.00, 14, 45, 13, '2020-03-26 11:39:02', '2020-03-26 11:39:02'),
	(46, 50.00, 14, 24, 11, '2020-03-26 11:39:45', '2020-03-26 11:39:45'),
	(47, 2.00, 14, 31, 13, '2020-03-26 11:39:56', '2020-03-26 11:39:56'),
	(48, 0.50, 14, 44, 17, '2020-03-26 11:40:08', '2020-03-26 11:40:08'),
	(49, 0.50, 14, 20, 10, '2020-03-26 11:40:23', '2020-03-26 11:40:23'),
	(50, 1.00, 14, 22, 14, '2020-03-26 11:40:32', '2020-03-26 11:40:32'),
	(51, 1.00, 14, 23, 14, '2020-03-26 11:40:42', '2020-03-26 11:40:42'),
	(52, 4.00, 14, 46, 13, '2020-03-26 11:42:12', '2020-03-26 11:42:12'),
	(53, 1.00, 14, 4, 13, '2020-03-26 11:42:17', '2020-03-26 11:42:17'),
	(54, 1.00, 14, 47, 13, '2020-03-26 11:42:25', '2020-03-26 11:42:25'),
	(55, 4.00, 14, 48, 10, '2020-03-26 11:42:39', '2020-03-26 11:42:39'),
	(56, 1.00, 14, 49, 13, '2020-03-26 11:42:56', '2020-03-26 11:42:56'),
	(57, 1.00, 16, 8, 13, '2020-03-26 11:54:41', '2020-03-26 11:54:41'),
	(58, 100.00, 16, 50, 11, '2020-03-26 11:54:59', '2020-03-26 11:55:05'),
	(59, 5.00, 16, 31, 13, '2020-03-26 11:55:17', '2020-03-26 11:55:17'),
	(60, 5.00, 16, 21, 3, '2020-03-26 11:55:30', '2020-03-26 11:55:30'),
	(61, 1.00, 16, 51, 18, '2020-03-26 11:55:41', '2020-03-26 11:55:41'),
	(62, 5.00, 16, 52, 19, '2020-03-26 11:56:00', '2020-03-26 11:56:00'),
	(63, 1.00, 16, 23, 14, '2020-03-26 11:56:10', '2020-03-26 11:56:10'),
	(64, 1.00, 16, 22, 14, '2020-03-26 11:56:19', '2020-03-26 13:43:12'),
	(66, 0.25, 17, 53, 20, '2020-03-26 13:52:51', '2020-03-26 13:52:51'),
	(67, 1.00, 17, 23, 14, '2020-03-26 13:55:39', '2020-03-26 13:55:39'),
	(68, 12.00, 17, 54, 3, '2020-03-26 13:55:48', '2020-03-26 13:55:48'),
	(69, 70.00, 17, 55, 11, '2020-03-26 13:56:00', '2020-03-26 13:56:00'),
	(70, 450.00, 17, 56, 11, '2020-03-26 13:56:12', '2020-03-26 13:56:12'),
	(71, 1.20, 17, 57, 5, '2020-03-26 13:56:22', '2020-03-26 13:56:22'),
	(72, 4.00, 18, 8, 22, '2020-03-26 15:11:07', '2020-03-26 15:11:07'),
	(73, 4.00, 18, 55, 13, '2020-03-26 15:11:16', '2020-03-26 15:11:16'),
	(74, 20.00, 18, 59, 3, '2020-03-26 15:11:26', '2020-03-26 15:11:26'),
	(75, 1.00, 18, 39, 9, '2020-03-26 15:11:42', '2020-03-26 15:11:42'),
	(76, 1.00, 18, 24, 9, '2020-03-26 15:11:50', '2020-03-26 15:11:50'),
	(77, 1.00, 18, 21, 9, '2020-03-26 15:11:58', '2020-03-26 15:11:58'),
	(78, 1.00, 18, 60, 14, '2020-03-26 15:12:07', '2020-03-26 15:12:07');
/*!40000 ALTER TABLE `recette_ingredient_unites` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. type_ingredients
DROP TABLE IF EXISTS `type_ingredients`;
CREATE TABLE IF NOT EXISTS `type_ingredients` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.type_ingredients : ~2 rows (environ)
/*!40000 ALTER TABLE `type_ingredients` DISABLE KEYS */;
INSERT INTO `type_ingredients` (`id`, `nom`, `created_at`, `updated_at`) VALUES
	(1, 'Légume', '2020-03-14 20:24:24', '2020-03-14 20:25:49');
/*!40000 ALTER TABLE `type_ingredients` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. unites
DROP TABLE IF EXISTS `unites`;
CREATE TABLE IF NOT EXISTS `unites` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abreviation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.unites : ~16 rows (environ)
/*!40000 ALTER TABLE `unites` DISABLE KEYS */;
INSERT INTO `unites` (`id`, `nom`, `abreviation`, `created_at`, `updated_at`) VALUES
	(1, 'Litre', 'l', '2020-03-14 21:04:50', '2020-03-14 21:04:50'),
	(2, 'Décilitre', 'dl', '2020-03-14 21:05:26', '2020-03-14 21:05:26'),
	(3, 'Centilitre', 'cl', '2020-03-14 21:05:34', '2020-03-14 21:05:34'),
	(4, 'Millilitre', 'ml', '2020-03-14 21:05:44', '2020-03-14 21:05:44'),
	(5, 'Kilogramme', 'kg', '2020-03-14 21:06:16', '2020-03-14 21:07:44'),
	(8, 'Milligramme', 'mg', '2020-03-14 21:22:48', '2020-03-14 21:22:48'),
	(9, 'Cuillère à soupe', 'c. à s.', '2020-03-14 21:23:05', '2020-03-14 21:23:05'),
	(10, 'Cuillère à café', 'c. à c.', '2020-03-14 21:23:19', '2020-03-14 21:23:19'),
	(11, 'Gramme', 'g', '2020-03-14 21:24:10', '2020-03-14 21:24:10'),
	(12, 'Tranche', 'tranche', '2020-03-14 21:25:03', '2020-03-14 21:25:03'),
	(13, 'Unité', 'u', '2020-03-17 19:55:47', '2020-03-17 19:55:47'),
	(14, 'Pincée', 'pincée', '2020-03-20 12:59:17', '2020-03-20 12:59:17'),
	(15, 'Rouleau', 'rouleau', '2020-03-26 11:00:23', '2020-03-26 11:00:23'),
	(16, 'Sachet', 'sachet', '2020-03-26 11:02:01', '2020-03-26 11:02:01'),
	(17, 'Bouquet', 'bouquet', '2020-03-26 11:37:53', '2020-03-26 11:37:53'),
	(18, 'Branche', 'branche', '2020-03-26 11:53:51', '2020-03-26 11:53:51'),
	(19, 'Goutte', 'goutte', '2020-03-26 11:54:05', '2020-03-26 11:54:05'),
	(20, 'Botte', 'botte', '2020-03-26 13:51:28', '2020-03-26 13:51:28'),
	(22, 'Filet', 'filet', '2020-03-26 15:09:58', '2020-03-26 15:09:58');
/*!40000 ALTER TABLE `unites` ENABLE KEYS */;

-- Listage de la structure de la table liste-de-courses. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table liste-de-courses.users : ~3 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `admin`) VALUES
	(1, 'Admin', 'admin@admin.com', NULL, '$2y$10$Kaltn8CVHWMCMPZcHl.E7O/a4qr6bjuCCFWyZI4z3Aca9M4aUWo3y', 'Jl1qi2UEZPUhwb1jTenDv9Ym9j04o4FnrCupUt3DUmxUXqo1QoVU2AaaMNCs', '2020-03-15 15:23:21', '2020-03-15 19:22:13', 1),
	(3, 'BAEJAIFAIM', 'emilie.vassout.asol@gmail.com', NULL, '$2y$10$3paNO6tOL01ibRyu3NGcmOmfPniVFaGUMJLOe1k.AkA2axR8XI5om', NULL, '2020-03-21 16:55:22', '2020-03-21 16:55:22', 0),
	(4, 'user', 'user@user.com', NULL, '$2y$10$EoY89IqUIloL3HWPrplktezNV6uyB.S/bWo/8hXLD86WajxOuYa1S', NULL, '2020-03-26 14:40:26', '2020-03-26 14:40:26', 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
