<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Votre mot de passe a bien été réinitialisé !',
    'sent' => 'Nous avons bien envoyés votre lien de réinitialisation !',
    'throttled' => 'Merci d\'attendre avant de réessayer.',
    'token' => 'Ce lien de réinitialisation est incorrect.',
    'user' => "Nous ne trouvons pas d'utilisateur avec cette adresse email.",

];
