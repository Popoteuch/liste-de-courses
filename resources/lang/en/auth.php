<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Adresse email ou mot de passe incorrect(s).',
    'throttle' => 'Trop de tentatives de connexions. Veuillez réessayer dans :seconds secondes.',

];
