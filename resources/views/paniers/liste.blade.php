@extends('layouts.default')

@section('content')
<h2>Liste de courses</h2>
	@if (isset($paniers) && count($paniers) != 0)
	<div>
		<table class="table table-striped table-hover text-center">
			<thead class="thead-dark">
				<tr>
					<th scope="col">Nom</th>
					<th scope="col">Quantité</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($paniers as $panier)
				<tr>
					<td>{{ $panier->nom_ingredient }}</td>
					<td>{!! $panier->quantite !!}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div>
		Liste de courses vide !
	</div>
	@endif
@endsection

@section('titre', $titre)
