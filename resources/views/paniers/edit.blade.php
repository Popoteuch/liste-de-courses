@extends('layouts.default')

@section('content')
	<h2>{{ $panier->nom_recette }}</h2>
	{!! Form::open(['method' => 'put', 'url' => route('panier.update', $panier->id)]) !!}
		<div class="form-group">
			{!! Form::label('personnes', 'Personnes : ') !!}
			{!! Form::text('personnes', $panier->personnes, ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
