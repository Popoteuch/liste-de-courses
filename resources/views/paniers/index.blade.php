@extends('layouts.default')

@section('content')
<h2>Détails du panier</h2>

	@if (isset($paniers) && count($paniers) != 0)
	{!! Form::open(['url' => route('panier.destroyAll'), 'method' => 'POST', 'class' => 'd-flex justify-content-end']) !!}
		{{ Form::hidden('_method', 'DELETE') }}
		{{ Form::submit('Vider le panier', ['class' => 'btn btn-danger float-right'])}}
	{!! Form::close() !!}
	<div>
		<table class="table table-striped table-hover text-center">
			<thead class="thead-dark">
				<tr>
					<th scope="col">Nom</th>
					<th scope="col">Personnes</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($paniers as $panier)
				<tr>
					<td><a href="{{ route('recette.show', $panier->recette) }}">{{ $panier->nom_recette }}</a></td>
					<td>{{ $panier->personnes }}</td>
					<td>
						<div>
							<a class="d-inline-block btn btn-success" href="{{ route('panier.edit', ['panier' => $panier->id]) }}">Modifier</a>
							{!! Form::open(['action' => ['Admin\PanierController@destroy', 'panier' => $panier->id], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Supprimer', ['class' => 'btn btn-danger'])}}
							{!! Form::close() !!}
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div>
		Panier vide !
	</div>
	@endif
@endsection

@section('titre', $titre)
