@extends('layouts.default')

@section('content')
	<h2>Ajouter une nouvelle unité</h2>
	{!! Form::open(['url' => route('unite.store')]) !!}
		<div class="form-group">
			{!! Form::label('nom', 'Nom : ') !!}
			{!! Form::text('nom', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('abreviation', 'Abréviation : ') !!}
			{!! Form::text('abreviation', null, ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Ajouter</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
