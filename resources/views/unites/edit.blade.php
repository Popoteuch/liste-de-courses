@extends('layouts.default')

@section('content')
	<h2>Editer l'unité</h2>
	{!! Form::open(['method' => 'put', 'url' => route('unite.update', $unite)]) !!}
		<div class="form-group">
			{!! Form::label('nom', 'Nom : ') !!}
			{!! Form::text('nom', $unite['nom'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('abreviation', 'Abréviation : ') !!}
			{!! Form::text('abreviation', $unite['abreviation'], ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
