@extends('layouts.default')

@section('content')
	<h2>Liste des unités</h2>
	<div class="d-flex justify-content-between mt-3">
		{!! Form::open(['url' => 'admin/unite/recherche', 'method' => 'POST', 'class' => 'form-inline mt-2 mt-md-0']) !!}
			{{ Form::text('recherche', $recherche??null, ['class' => 'form-control', 'placeholder' => 'Unité']) }}
			{{ Form::submit('Rechercher', ['class' => 'btn btn-outline-secondary my-2 my-sm-0'])}}
		{!! Form::close() !!}
		@if (Auth::check() && Auth::user()->isAdmin())
		<a class="btn btn-primary" href="{{ route('unite.create') }}">Ajouter une unité</a>
		@endif
	</div>
	@if (isset($unites) && count($unites) != 0)
	<div>
		<table class="table table-striped table-hover text-center">
			<thead class="thead-dark">
				<tr>
					@if (Auth::check() && Auth::user()->isAdmin())
					<th scope="col">Id</th>
					@endif
					<th scope="col">Nom</th>
					<th scope="col">Abréviation</th>
					@if (Auth::check() && Auth::user()->isAdmin())
					<th scope="col">Actions</th>
					@endif
				</tr>
			</thead>
			<tbody>
			@foreach ($unites as $unite)
				<tr>
					@if (Auth::check() && Auth::user()->isAdmin())
					<td>{{ $unite['id'] }}</td>
					@endif
					<td>{{ $unite['nom'] }}</td>
					<td>{{ $unite['abreviation'] }}</td>
					@if (Auth::check() && Auth::user()->isAdmin())
					<td>
						<div>
							<a class="d-inline-block btn btn-success" href="{{ route('unite.edit', ['unite' => $unite]) }}">Modifier</a>
							{!! Form::open(['action' => ['Admin\UniteController@destroy', 'unite' => $unite], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Supprimer', ['class' => 'btn btn-danger'])}}
							{!! Form::close() !!}
						</div>
					</td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div>
		Aucune unité dans la base de donnée !
	</div>
	@endif
@endsection

@section('titre', $titre)
