@extends('layouts.default')

@section('content')
	<h2>Éditer l'étape</h2>
	{!! Form::open(['method' => 'put', 'url' => route('recette_etape.update', $etape), 'class' => 'mb-2']) !!}
		{!! Form::text('recette', $etape['recette'], ['hidden' => 'hidden', 'readonly' => 'readonly']) !!}
		<div class="form-group">
			{!! Form::label('etape', 'Étape : ') !!}
			{!! Form::number('etape', $etape['etape'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description : ') !!}
			{!! Form::textArea('description', $etape['description'], ['class' => 'form-control', 'rows' => 3]) !!}
		</div>
		<button class="btn btn-success">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
