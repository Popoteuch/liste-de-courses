<h2>Étapes</h2>
<div class="mb-2">
	@forelse ($etapes as $num_etape => $etape)
	<div class="card mb-1">
		<div class="card-header">
			<h5 class="float-left">
				Étape n°{{ $etape['etape'] }}
			</h5>
		</div>
		<div class="card-body text-left">
			<p class="card-text">{{ $etape['description'] }}</p>
		</div>
	</div>
	@empty
	<ul>
		<div class="mb-3">
			Aucune étape pour cette recette !
		</div>
	</ul>
	@endforelse
</div>
