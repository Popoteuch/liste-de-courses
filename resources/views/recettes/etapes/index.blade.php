<h2>Étapes</h2>
<div class="mb-2">
	@forelse ($etapes as $num_etape => $etape)
	<div class="card mb-1">
		<div class="card-header">
			<h4 class="float-left">
				Étape n°{{ $etape['etape'] }}
			</h4>
			<span class="float-right">
				<a href="{{ route('recette_etape.edit', $etape) }}" class="btn btn-success">Modifier</a>
				{!! Form::open(['action' => ['RecetteEtapeController@destroy', $etape], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Supprimer', ['class' => 'btn btn-danger'])}}
				{!! Form::close() !!}
			</span>
		</div>
		<div class="card-body text-left">
			<p class="card-text">{{ $etape['description'] }}</p>
		</div>
	</div>
	@empty
	<ul>
		<div class="mb-3">
			Aucune étape pour cette recette !
		</div>
	</ul>
	@endforelse
</div>
<a href="{{ url('recette_etape/'.$recette['id'].'/create') }}" type="button" class="btn btn-primary">Ajouter une étape</a>
