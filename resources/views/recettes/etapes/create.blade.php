@extends('layouts.default')

@section('content')
	<h2>Ajouter une nouvelle étape</h2>
	{!! Form::open(['url' => route('recette_etape.store'), 'recette' => $recette]) !!}
		{!! Form::text('recette', $recette['id'], ['hidden' => 'hidden', 'readonly' => 'readonly']) !!}
		<div class="form-group">
			{!! Form::label('etape', 'Étape : ') !!}
			{!! Form::number('etape', $etape??1, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description : ') !!}
			{!! Form::textArea('description', null, ['class' => 'form-control', 'rows' => 3]) !!}
		</div>
		<button class="btn btn-primary">Ajouter</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
