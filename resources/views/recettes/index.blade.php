@extends('layouts.default')

@section('titre', $titre)

@section('content')
	<h2>Liste des recettes</h2>
	<div class="d-flex justify-content-between mt-3">
		{!! Form::open(['url' => 'recette/recherche', 'method' => 'POST', 'class' => 'form-inline mt-2 mt-md-0']) !!}
			{{ Form::text('recherche', $recherche??null, ['class' => 'form-control', 'placeholder' => 'Recette']) }}
			{{ Form::submit('Rechercher', ['class' => 'btn btn-outline-secondary my-2 my-sm-0'])}}
		{!! Form::close() !!}
	</div>
	@if (isset($recettes) && count($recettes) != 0)
	<main role="main">
		<div class="album py-5">
			<div class="container">
				<div class="row">
				@if (Auth::check() && Auth::user()->isAdmin())
					<div class="col-md-4">
						<a class="card mb-4 box-shadow" href="{{ route('recette.create') }}">
						<img style="height: {{ $height_max }}px; width: {{ $height_max }}px" class="mx-auto figure-img img-thumbnail border-0 rounded card-img-top" src="http://{{ request()->getHost() }}/assets/images/plus.png" alt="Ajouter une recette">
						  <div class="card-body">
							<p class="card-text text-center">
								Ajouter une recette
							</p>
						  </div>
						</a>
					</div>
				@endif
				@foreach ($recettes as $recette)
					<div class="col-md-4">
						<div class="card mb-4 box-shadow">
						<a href="{{ route('recette.show', $recette) }}" style="height: {{ $tailles_images[$recette['id']]['height'] }}px;" class="mx-auto">
							<img style="width: {{ $tailles_images[$recette['id']]['width'] }}px;height: {{ $tailles_images[$recette['id']]['height'] }}px;" class="mx-auto figure-img img-thumbnail border-0 rounded card-img-top" src="http://{{ request()->getHost() }}/images/{{ $recette['image'] }}" alt="{{ $recette['nom'] }}">
						</a>
						<div class="card-body">
							<h5 class="card-title">
							{{ $recette['nom'] }}
							(
								@for ($i = 0; $i < $recette['personnes']; $i++)
									<i class="fas fa-user"></i>
								@endfor
							)
							</h5>
							<p class="card-text">
								{{ $recette['description'] }}
							</p>
							<div class="d-flex justify-content-between align-items-center">
								<div >
									<a href="{{ route('recette.show', $recette) }}" type="button" class="btn btn-sm btn-outline-secondary">Voir</a>
									@if (Auth::check() && Auth::user()->isAdmin())
										<a href="{{ route('recette.edit', $recette) }}" type="button" class="btn btn-sm btn-outline-success">Modifier</a>
										{!! Form::open(['action' => ['RecetteController@destroy', $recette], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
											{{ Form::hidden('_method', 'DELETE') }}
											{{ Form::submit('Supprimer', ['class' => 'btn btn-sm btn-outline-danger rounded-right'])}}
										{!! Form::close() !!}
									@endif
								</div>
								<small class="text-muted">{{ $recette['created_at']->diffForHumans() }}</small>
							</div>
						</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
		</div>
	</main>
	@else
	<div>
		Aucune recette dans la base de donnée !
	</div>
	@endif
@endsection
