@extends('layouts.default')

@section('content')
<h2>Editer la recette</h2>
<div class='row'>
	<div class='col-6'>
		{!! Form::open(['method' => 'put', 'url' => route('recette.update', $recette), 'class' => 'mb-2', "enctype" => "multipart/form-data"]) !!}
			<div class="form-group">
				{!! Form::label('nom', 'Nom : ') !!}
				{!! Form::text('nom', $recette['nom'], ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description : ') !!}
				{!! Form::textArea('description', $recette['description'], ['class' => 'form-control', 'rows' => 2]) !!}
			</div>
			<div class="form-group">
				{!! Form::label('temps', 'Temps : ') !!}
				{!! Form::text('temps', $recette['temps'], ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('personnes', 'Personnes : ') !!}
				{!! Form::number('personnes', $recette['personnes'], ['class' => 'form-control']) !!}
			</div>
			<div class="form-group">
				<div class="custom-file">
					{!! Form::file('image', ['class' => 'custom-file-input']) !!}
					{!! Form::label('image', null, ['class' => 'custom-file-label']) !!}
				</div>
			</div>
			<button class="btn btn-success">Modifier</button>
		{!! Form::close() !!}
	</div>
	<div class="col-6 d-block">
		<div class='align-middle' style='width: {{ $tailles_image['width'] }}px;'>
			<img style="width: {{ $tailles_image['width'] }}px;height: {{ $tailles_image['height'] }}px;" class="mx-auto figure-img img-thumbnail border-0 rounded card-img-top" src="http://{{ request()->getHost() }}/images/{{ $recette['image'] }}" alt="{{ $recette['nom'] }}">
		</div>
	</div>
</div>
	@include('recettes.ingredients.index')
	@include('recettes.etapes.index')
@endsection

@section('titre', $titre)
