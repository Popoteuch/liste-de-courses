<h2>Ingrédients</h2>
<div class="mb-2">
	<ul>
		@forelse ($recette_ingredient_unites as $num_ingredient => $recette_ingredient_unite)
		<li class='row'>
			<div class='col'>
				{{ $recette_ingredient_unite->nom_ingredient }} : 
			</div>
			<div class='col'>
				{{ $recette_ingredient_unite->quantite . ' ' . $recette_ingredient_unite->abr_unite }}
			</div>
			<div class='col'>
				<a href="{{ route('recette_ingredient_unite.edit', $recette_ingredient_unite->id) }}" class="btn btn-success btn-sm">
					Modifier
					<i class="fas fa-edit"></i>
				</a>
				{!! Form::open(['action' => ['RecetteIngredientUniteController@destroy', $recette_ingredient_unite->id], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
					{{ Form::hidden('_method', 'DELETE') }}
					{{ Form::submit('Supprimer X', ['class' => 'btn btn-danger btn-sm'])}}
				{!! Form::close() !!}
			</div>
		</li>
		@empty
		<div class="mb-3">
			Aucune ingrédient pour cette recette !
		</div>
		@endforelse
	</ul>
</div>
<a href="{{ url('recette_ingredient_unite/'.$recette['id'].'/create') }}" type="button" class="btn btn-primary mb-3">Ajouter un ingrédient</a>
