@extends('layouts.default')

@section('content')
	<h2>Ajouter un ingrédient à la recette</h2>
	{!! Form::open(['url' => route('recette_ingredient_unite.store'), 'recette' => $recette]) !!}
		{!! Form::text('recette', $recette, ['hidden' => 'hidden', 'readonly' => 'readonly']) !!}
		<div class="form-group">
			{!! Form::label('ingredient', 'Ingrédient : ') !!}
			{!! Form::select('ingredient', $ingredients, null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('quantite', 'Quantité : ') !!}
			{!! Form::text('quantite', "1.00", ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('unite', 'Unité : ') !!}
			{!! Form::select('unite', $unites, null, ['class' => 'form-control', 'rows' => 3]) !!}
		</div>
		<button class="btn btn-primary">Ajouter</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
