@extends('layouts.default')

@section('content')
	<h2>Éditer l'ingrédient</h2>
	{!! Form::open(['method' => 'put', 'url' => route('recette_ingredient_unite.update', $recette_ingredient_unite), 'class' => 'mb-2']) !!}
		{!! Form::text('recette', $recette_ingredient_unite['recette'], ['hidden' => 'hidden', 'readonly' => 'readonly']) !!}
		<div class="form-group">
			{!! Form::label('ingredient', 'Ingrédient : ') !!}
			{!! Form::select('ingredient', $ingredients, $recette_ingredient_unite['ingredient'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('quantite', 'Quantité : ') !!}
			{!! Form::text('quantite', $recette_ingredient_unite['quantite'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('unite', 'Unité : ') !!}
			{!! Form::select('unite', $unites, $recette_ingredient_unite['unite'], ['class' => 'form-control', 'rows' => 3]) !!}
		</div>
		<button class="btn btn-success">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
