<h2>Ingrédients</h2>
<div class="mb-2 container">
	<div class='row row-cols-2'>
		@forelse ($recette_ingredient_unites as $num_ingredient => $recette_ingredient_unite)
		<div class='col row'>
			<div class='col-6 float-right'>
				{{ $recette_ingredient_unite->nom_ingredient }} : 
			</div>
			<div class='col-6'>
				{{ $recette_ingredient_unite->quantite . ' ' . $recette_ingredient_unite->abr_unite }}
			</div>
		</div>
		@empty
		<div class="mb-3">
			Aucune ingrédient pour cette recette !
		</div>
		@endforelse
	</div>
</div>
