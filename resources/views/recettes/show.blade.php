@extends('layouts.default')

@section('content')
	<div>
		<h2 class='mb-3 d-flex align-items-center justify-content-center'>
			{{ $recette['nom'] }} 
			(
				@for ($i = 0; $i < $recette['personnes']; $i++)
					<i class="fas fa-user"></i>
				@endfor
			)
			- {{ $recette['temps'] }}
		</h2>
	</div>
	<div>
		<div class="mb-2 d-block">
			<div class='mx-auto' style='width: {{ $tailles_image['width'] }}px;'>
				<img style="width: {{ $tailles_image['width'] }}px;height: {{ $tailles_image['height'] }}px;" class="mx-auto figure-img img-thumbnail border-0 rounded card-img-top" src="http://{{ request()->getHost() }}/images/{{ $recette['image'] }}" alt="{{ $recette['nom'] }}">
			</div>
			<div class='text-center'>
				<i>{{ $recette['description'] }}</i>
			</div>
		</div>
	</div>
	@include('recettes.ingredients.show')
	@include('recettes.etapes.show')
	@if (Auth::check())
		{!! Form::open(['method' => 'POST', 'url' => 'admin/panier/'.$recette['id'], 'class' => 'form-inline']) !!}
			<div class="form-group mx-sm-3 mb-2">
				{!! Form::label('personnes', 'Personnes : ', ['class' => 'form-group mb-2 mr-2']) !!}
				{!! Form::number('personnes', $recette['personnes'], ['class' => 'form-control']) !!}
			</div>
			<button class="btn btn-primary mb-2">Ajouter au panier</button>
		{!! Form::close() !!}
	@endif
@endsection

@section('titre', $titre)
