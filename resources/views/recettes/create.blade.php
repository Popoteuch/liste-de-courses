@extends('layouts.default')

@section('content')
	<h2>Ajouter une nouvelle recette</h2>
	{!! Form::open(['url' => route('recette.store'), "enctype" => "multipart/form-data"]) !!}
		<div class="form-group">
			{!! Form::label('nom', 'Nom : ') !!}
			{!! Form::text('nom', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('description', 'Description : ') !!}
			{!! Form::textArea('description', null, ['class' => 'form-control', 'rows' => 1]) !!}
		</div>
		<div class="form-group">
			{!! Form::label('temps', 'Temps : ') !!}
			{!! Form::text('temps', null, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('personnes', 'Personnes : ') !!}
			{!! Form::number('personnes', 0, ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			<div class="custom-file">
				{!! Form::file('image', ['class' => 'custom-file-input']) !!}
				{!! Form::label('image', null, ['class' => 'custom-file-label']) !!}
			</div>
		</div>
		<button class="btn btn-primary">Ajouter</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
