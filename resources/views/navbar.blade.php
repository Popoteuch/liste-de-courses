<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
	<a class="navbar-brand" href="{{ route('panier.liste')}}">Ma liste de courses</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarsExampleDefault">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item{{ (isset($titre) && $titre == "Recettes")?" active":"" }}">
				<a class="nav-link" href="{{ route('recette.index')}}">
					<i class="fas fa-hamburger"></i>
					RECETTES
				</a>
			</li>
			@if (Auth::check())
				<li class="nav-item{{ (isset($titre) && $titre == "Ingrédients")?" active":"" }}">
					<a class="nav-link" href="{{ route('ingredient.index')}}">
						<i class="fas fa-apple-alt"></i>
						INGREDIENTS
					</a>
				</li>
				<li class="nav-item{{ (isset($titre) && $titre == "Unités")?" active":"" }}">
					<a class="nav-link" href="{{ route('unite.index')}}">
						<i class="fas fa-weight"></i>
						UNITES
					</a>
				</li>
			@endif
		</ul>
		@if (Auth::check())
		<ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
			<li class="nav-item{{ (isset($titre) && $titre == "Panier")?" active":"" }}">
				<a class="nav-link" href="{{ route('panier.index')}}">
					<i class="fas fa-shopping-cart"></i>
					MON PANIER
				</a>
			</li>
			@if (Auth::user()->isAdmin())
			<li class="nav-item{{ (isset($titre) && $titre == "Administration")?" active":"" }}">
				<a class="nav-link" href="{{ route('user.index')}}">
					<i class="fas fa-users"></i>
					ADMINISTRATION
				</a>
			</li>
			@endif
			<li class="nav-item{{ (isset($titre) && $titre == "Utilisateur")?" active":"" }}">
				<a class="nav-link" href="{{ route('user.show', Auth::id())}}">
					<i class="fas fa-user-circle"></i>
					MON COMPTE
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" 
					href="{{ route('deconnexion') }}"
					onclick="event.preventDefault();
							 document.getElementById('logout-form').submit();">
					<i class="fas fa-power-off"></i>
					DECONNEXION
				</a>
				<form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</li>
		</ul>
		@else
		<ul class="navbar-nav flex-row ml-md-auto d-none d-md-flex">
			<li class="nav-item{{ (isset($titre) && $titre == "Inscription")?" active":"" }}">
				<a class="nav-link" href="{{ route('register')}}">
					<i class="fas fa-user-plus"></i>
					INSCRIPTION
				</a>
			</li>
			<li class="nav-item{{ (isset($titre) && $titre == "Connexion")?" active":"" }}">
				<a class="nav-link" href="{{ route('login')}}">
					<i class="fas fa-sign-in-alt"></i>
					CONNEXION
				</a>
			</li>
		</ul>
		@endif
	</div>
</nav>
