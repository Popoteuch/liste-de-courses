@extends('layouts.default')

@section('content')
	<h2>Editer l'utilisateur</h2>
	{!! Form::open(['method' => 'put', 'url' => route('user.update', $user)]) !!}
		<div class="form-group">
			{!! Form::label('name', 'Nom : ') !!}
			{!! Form::text('name', $user['name'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('email', 'Email : ') !!}
			{!! Form::text('email', $user['email'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('admin', 'Administrateur (0 ou 1) : ') !!}
			{!! Form::number('admin', $user['admin'], ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
