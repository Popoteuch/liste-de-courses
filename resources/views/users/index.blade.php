@extends('layouts.default')

@section('content')
	<h2>Liste des utilisateurs</h2>
	<div class="d-flex justify-content-between mt-3">
		{!! Form::open(['url' => 'admin/user/recherche', 'method' => 'POST', 'class' => 'form-inline mt-2 mt-md-0']) !!}
			{{ Form::text('recherche', $recherche??null, ['class' => 'form-control', 'placeholder' => 'Utilisateur']) }}
			{{ Form::submit('Rechercher', ['class' => 'btn btn-outline-secondary my-2 my-sm-0'])}}
		{!! Form::close() !!}
	</div>
	@if (isset($users) && count($users) != 0)
	<div>
		<table class="table table-striped table-hover text-center">
			<thead class="thead-dark">
				<tr>
					<th scope="col">Id</th>
					<th scope="col">Nom</th>
					<th scope="col">Email</th>
					<th scope="col">Administrateur</th>
					<th scope="col">Actions</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($users as $user)
				<tr>
					<td>{{ $user['id'] }}</td>
					<td>{{ $user['name'] }}</td>
					<td>{{ $user['email'] }}</td>
					<td>{{ $user['admin']?"Oui":"Non" }}</td>
					@if (Auth::check() && Auth::user()->isAdmin())
					<td>
						<div>
							<a class="d-inline-block btn btn-success" href="{{ route('user.edit', ['user' => $user]) }}">Modifier</a>
							{!! Form::open(['action' => ['Admin\UserController@destroy', 'user' => $user], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Supprimer', ['class' => 'btn btn-danger'])}}
							{!! Form::close() !!}
						</div>
					</td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div>
		Aucun utilisateur dans la base de donnée !
	</div>
	@endif
@endsection

@section('titre', $titre)
