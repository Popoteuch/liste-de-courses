@extends('layouts.default')

@section('titre', $titre)

@section('content')
	<h2>Mon compte</h2>
	{!! Form::open(['method' => 'put', 'url' => route('user.update', $user)]) !!}
		<div class="form-group">
			{!! Form::label('name', 'Nom : ') !!}
			{!! Form::text('name', $user['name'], ['class' => 'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('email', 'Adresse email : ') !!}
			{!! Form::text('email', $user['email'], ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Modifier</button>
	{!! Form::close() !!}
@endsection
