@extends('layouts.default')

@section('content')
	<h2>Ajouter un nouvel ingrédient</h2>
	{!! Form::open(['url' => route('ingredient.store')]) !!}
		<div class="form-group">
			{!! Form::label('nom', 'Nom : ') !!}
			{!! Form::text('nom', null, ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Ajouter</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
