@extends('layouts.default')

@section('content')
	<h2>Liste des ingrédients</h2>
	<div class="d-flex justify-content-between mt-3">
		{!! Form::open(['url' => 'admin/ingredient/recherche', 'method' => 'POST', 'class' => 'form-inline mt-2 mt-md-0']) !!}
			{{ Form::text('recherche', $recherche??null, ['class' => 'form-control', 'placeholder' => 'Ingrédient']) }}
			{{ Form::submit('Rechercher', ['class' => 'btn btn-outline-secondary my-2 my-sm-0'])}}
		{!! Form::close() !!}
		@if (Auth::check() && Auth::user()->isAdmin())
		<a class="btn btn-primary" href="{{ route('ingredient.create') }}">Ajouter un ingrédient</a>
		@endif
	</div>
	@if (isset($ingredients) && count($ingredients) != 0)
	<div>
		<table class="table table-striped table-hover text-center">
			<thead class="thead-dark">
				<tr>
					@if (Auth::check() && Auth::user()->isAdmin())
					<th scope="col">Id</th>
					@endif
					<th scope="col">Nom</th>
					@if (Auth::check() && Auth::user()->isAdmin())
					<th scope="col">Actions</th>
					@endif
				</tr>
			</thead>
			<tbody>
			@foreach ($ingredients as $ingredient)
				<tr>
					@if (Auth::check() && Auth::user()->isAdmin())
					<td>{{ $ingredient['id'] }}</td>
					@endif
					<td>{{ $ingredient['nom'] }}</td>
					@if (Auth::check() && Auth::user()->isAdmin())
					<td>
						<div>
							<a class="d-inline-block btn btn-success" href="{{ route('ingredient.edit', ['ingredient' => $ingredient]) }}">Modifier</a>
							{!! Form::open(['action' => ['Admin\IngredientController@destroy', 'ingredient' => $ingredient], 'method' => 'POST', 'class' => 'd-inline-block']) !!}
								{{ Form::hidden('_method', 'DELETE') }}
								{{ Form::submit('Supprimer', ['class' => 'btn btn-danger'])}}
							{!! Form::close() !!}
						</div>
					</td>
					@endif
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
	@else
	<div>
		Aucun ingrédient dans la base de donnée !
	</div>
	@endif
@endsection

@section('titre', $titre)
