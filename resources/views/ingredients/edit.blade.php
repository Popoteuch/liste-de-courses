@extends('layouts.default')

@section('content')
	<h2>Editer l'ingrédient</h2>
	{!! Form::open(['method' => 'put', 'url' => route('ingredient.update', $ingredient)]) !!}
		<div class="form-group">
			{!! Form::label('nom', 'Nom : ') !!}
			{!! Form::text('nom', $ingredient['nom'], ['class' => 'form-control']) !!}
		</div>
		<button class="btn btn-primary">Modifier</button>
	{!! Form::close() !!}
@endsection

@section('titre', $titre)
