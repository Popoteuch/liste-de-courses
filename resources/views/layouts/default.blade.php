
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
		<meta name="generator" content="Jekyll v3.8.6">
		<title>Ma liste de courses</title>

		<link rel="canonical" href="https://getbootstrap.com/docs/4.4/examples/starter-template/">

		<!-- Bootstrap core CSS -->
		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
		<script src="https://kit.fontawesome.com/159b3a28ae.js" crossorigin="anonymous"></script>
	</head>
	<body style="padding-top: 50px;" background="{{ url("assets/images/background.jpg ")}}">
		@include('navbar')

		<main role="main" class="container bg-white text-dark">
			<div class="starter-template pt-4 pb-4">
				<h1>
					@yield('titre')
				</h1>
				@yield('content')
			</div>

		</main><!-- /.container -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		<script>window.jQuery || document.write('<script src="/docs/4.4/assets/js/vendor/jquery.slim.min.js"><\/script>')</script><script src="/docs/4.4/dist/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
	</body>
</html>
