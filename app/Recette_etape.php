<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recette_etape extends Model
{
    protected $fillable = ['recette', 'etape', 'description'];

    public function scopeEtapeAscending($query)
    {
        return $query->orderBy('etape','ASC');
    }
}
