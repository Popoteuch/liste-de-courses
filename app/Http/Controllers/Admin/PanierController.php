<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Recette;
use App\Panier;
use App\Unite;
use DB;

class PanierController extends Controller
{
    protected $titre = "Panier";

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function ajouteRecette($recette, $personnes)
    {
        $recette = Panier::create(['user' => Auth::id(), 'recette' => $recette, 'personnes' => $personnes]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titre = $this->titre;
        $paniers = DB::table('paniers')
            ->join('recettes', 'recettes.id', '=', 'paniers.recette')
            ->select('paniers.*', 'recettes.nom as nom_recette')
            ->where('paniers.user', Auth::id())
            ->get();
        return view('paniers.index', compact('paniers', 'titre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $recette)
    {
        $recette = Panier::create(['user' => Auth::id(), 'recette' => $recette, 'personnes' => $request->personnes]);
        return redirect(route('recette.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Panier  $panier
     * @return \Illuminate\Http\Response
     */
    public function show(Panier $panier)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Panier  $panier
     * @return \Illuminate\Http\Response
     */
    public function edit($panier)
    {
        $titre = $this->titre;
        $panier = DB::table('paniers')
            ->join('recettes', 'recettes.id', '=', 'paniers.recette')
            ->select('paniers.*', 'recettes.nom as nom_recette')
            ->where('paniers.id', $panier)
            ->first();
        return view('paniers.edit', compact('panier', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Panier  $panier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $panier)
    {
        Panier::where('id', $panier)->update(['personnes' => $request->personnes]);
        return redirect(route('panier.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Panier  $panier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Panier $panier)
    {
        Panier::where('user', Auth::id())->where('id', $panier->id)->first()->delete();
        return redirect(route('panier.index'));
    }

    public function destroyAll()
    {
        Panier::where('user', Auth::id())->delete();
        return redirect(route('panier.index'));
    }

    public function liste()
    {
        $titre = $this->titre;
        $paniers = DB::table('paniers')
            ->join('recettes', 'recettes.id', '=', 'paniers.recette')
            ->join('recette_ingredient_unites', 'recette_ingredient_unites.recette', '=', 'recettes.id')
            ->join('ingredients', 'ingredients.id', '=', 'recette_ingredient_unites.ingredient')
            ->join('unites', 'unites.id', '=', 'recette_ingredient_unites.unite')
            ->select('paniers.*',
                'recettes.personnes as personnes_recette',
                'recette_ingredient_unites.quantite as quantite',
                'ingredients.id as id_ingredient',
                'ingredients.nom as nom_ingredient',
                'unites.id as id_unite',
                'unites.abreviation as abr_unite')
            ->where('paniers.user', Auth::id())
            ->orderBy('ingredients.nom', 'ASC')
            ->get()
            ->toArray();
        $dernier_ingredient = [];
        $dernier_element = array_key_last($paniers);
        foreach($paniers as $id_panier => $panier)
        {
            $quantite_recalculcee = $panier->quantite * ($panier->personnes / $panier->personnes_recette);

            if($dernier_ingredient != [] && $dernier_ingredient['id_ingredient'] != $panier->id_ingredient)
            { // On définit l'ancient ingrédient
                $paniers[$dernier_ingredient['id_panier']]->quantite = '<ul>';
                foreach($dernier_ingredient['quantite'] as $id_unite => $quantite)
                {
                    $abr_unite = Unite::where('id', $id_unite)->select('abreviation')->first();
                    $paniers[$dernier_ingredient['id_panier']]->quantite .= '
                        <li>
                        ' . $quantite . ' ' . $abr_unite['abreviation'] . '
                        </li>';
                }
                $paniers[$dernier_ingredient['id_panier']]->quantite .= '</ul>';
            }

            if($dernier_ingredient == [] || $dernier_ingredient['id_ingredient'] != $panier->id_ingredient)
            { // Nouvel ingrédient
                $dernier_ingredient = [
                    'id_ingredient' => $panier->id_ingredient,
                    'id_panier' => $id_panier,
                    'quantite' => [
                        $panier->id_unite => $quantite_recalculcee
                    ]
                ];
                if($dernier_element == $id_panier)
                {
                    $paniers[$dernier_ingredient['id_panier']]->quantite = '<ul>';
                    foreach($dernier_ingredient['quantite'] as $id_unite => $quantite)
                    {
                        $abr_unite = Unite::where('id', $id_unite)->select('abreviation')->first();
                        // $abr_unite['abreviation
                        $paniers[$dernier_ingredient['id_panier']]->quantite .= '
                            <li>
                            ' . $quantite . ' ' . $abr_unite['abreviation'] . '
                            </li>';
                    }
                    $paniers[$dernier_ingredient['id_panier']]->quantite .= '</ul>';
                }
            }
            else
            { // Même ingrédient que le précédent
                $dernier_ingredient['quantite'][$panier->id_unite] = ($dernier_ingredient['quantite'][$panier->id_unite]??0) + $quantite_recalculcee;
                unset($paniers[$id_panier]);
            }
        }
        return view('paniers.liste', compact('paniers', 'titre'));
    }
}
