<?php

namespace App\Http\Controllers\Admin;

use App\Unite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Http\Controllers\Controller;

class UniteController extends Controller
{
    protected $titre = "Unités";

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin')->only(['edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titre = $this->titre;
        $unites = Unite::orderBy('nom', 'ASC')->get();
        return view('unites.index', compact('unites', 'titre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titre = $this->titre;
        return view('unites.create', compact('titre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $unite = Unite::create(['nom' => $request->nom, 'abreviation' => $request->abreviation]);
        return redirect(route('unite.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Unite  $unite
     * @return \Illuminate\Http\Response
     */
    public function show(Unite $unite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Unite  $unite
     * @return \Illuminate\Http\Response
     */
    public function edit(Unite $unite)
    {
        $titre = $this->titre;
        return view('unites.edit', compact('unite', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Unite  $unite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unite $unite)
    {
        $unite->update($request->all());
        return redirect(route('unite.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Unite  $unite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Unite $unite)
    {
        $unite->delete();
        return redirect(route('unite.index'));
    }
    
    public function recherche(Request $request)
    {
        $titre = $this->titre;
        $recherche = $request->recherche;
        $unites = Unite::where('nom', 'LIKE', '%' . $recherche . '%')->orWhere('id', 'LIKE', $recherche)->orWhere('abreviation', 'LIKE', '%' . $recherche . '%')->orderBy('nom', 'ASC')->get();
        return view('unites.index', compact('unites', 'titre', 'recherche'));
    }
}
