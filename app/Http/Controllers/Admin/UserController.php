<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $titre = "Administration";

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin')->except('show', 'update');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titre = $this->titre;
        $users = User::orderBy('id', 'ASC')->get();
        return view('users.index', compact('users', 'titre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $titre = "Utilisateur";
        $user = User::find(Auth::id());
        return view('users.show', compact('user', 'titre'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $titre = $this->titre;
        return view('users.edit', compact('user', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect(route('user.index'));
    }
    
    public function recherche(Request $request)
    {
        $titre = $this->titre;
        $recherche = $request->recherche;
        $users = User::where('name', 'LIKE', '%' . $recherche . '%')->orWhere('id', 'LIKE', $recherche)->orWhere('email', 'LIKE', '%' . $recherche . '%')->orderBy('id', 'ASC')->get();
        return view('users.index', compact('users', 'titre', 'recherche'));
    }
}
