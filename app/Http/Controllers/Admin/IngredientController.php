<?php

namespace App\Http\Controllers\Admin;

use App\Ingredient;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IngredientController extends Controller
{
    protected $titre = "Ingrédients";

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isAdmin')->only(['store', 'create', 'edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titre = $this->titre;
        $ingredients = Ingredient::orderBy('nom', 'ASC')->get();
        return view('ingredients.index', compact('ingredients', 'titre'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titre = $this->titre;
        return view('ingredients.create', compact('titre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ingredient = Ingredient::create(['nom' => $request->nom]);
        return redirect(route('ingredient.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function show(Ingredient $ingredient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingredient $ingredient)
    {
        $titre = $this->titre;
        return view('ingredients.edit', compact('ingredient', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingredient $ingredient)
    {
        $ingredient->update($request->all());
        return redirect(route('ingredient.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingredient $ingredient)
    {
        $ingredient->delete();
        return redirect(route('ingredient.index'));
    }

    public function recherche(Request $request)
    {
        $titre = $this->titre;
        $recherche = $request->recherche;
        $ingredients = Ingredient::where('nom', 'LIKE', '%' . $recherche . '%')->orWhere('id', 'LIKE', $recherche)->orderBy('nom', 'ASC')->get();
        return view('ingredients.index', compact('ingredients', 'titre', 'recherche'));
    }
}
