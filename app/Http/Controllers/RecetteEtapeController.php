<?php

namespace App\Http\Controllers;

use App\Recette_etape;
use App\Recette;
use Illuminate\Http\Request;
use DB;

class RecetteEtapeController extends Controller
{
	protected $titre = "Recettes";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($recette)
    {
        $recette = Recette::find($recette);
        $etape = Recette_etape::select(DB::raw('coalesce(max(etape) + 1, 1) as etape'))->where('recette', 'LIKE', $recette->id)->first()->etape;
        $titre = $this->titre;
        return view('recettes.etapes.create', compact('recette', 'titre', 'etape'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$recette = Recette::find($request->recette);
        Recette_etape::create(['recette' => $request->recette, 'etape' => $request->etape, 'description' => $request->description]);
        return redirect(route('recette.edit', $recette));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recette_etape  $recette_etape
     * @return \Illuminate\Http\Response
     */
    public function show(Recette_etape $recette_etape)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recette_etape  $recette_etape
     * @return \Illuminate\Http\Response
     */
    public function edit(Recette_etape $recette_etape)
    {
        $titre = $this->titre;
        $etape = $recette_etape;
        return view('recettes.etapes.edit', compact('etape', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recette_etape  $recette_etape
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recette_etape $recette_etape)
    {
        $recette = Recette::find($recette_etape['recette']);
        $recette_etape->update($request->all());
        return redirect(route('recette.edit', $recette));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recette_etape  $recette_etape
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recette_etape $recette_etape)
    {
        $recette = Recette::find($recette_etape['recette']);
        $recette_etape->delete();
        return redirect(route('recette.edit', $recette));
    }
}
