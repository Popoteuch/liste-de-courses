<?php

namespace App\Http\Controllers;

use App\Recette;
use Illuminate\Http\Request;
use App\Image;
use App\Ingredient;
use App\Recette_ingredient_unite;
use App\Recette_etape;
use DB;

class RecetteController extends Controller
{
    protected $titre = "Recettes";
    protected $taille_max = 300;
    protected $taille_max_album = 250;

    public function __construct()
    {
        $this->middleware('isAdmin')->only(['edit', 'update', 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $titre = $this->titre;
        $height_max = $this->taille_max_album;
        $recettes = Recette::idDescending()->get();
        $tailles_images = [];
        foreach($recettes as $recette)
        {
            list($width, $height, $type, $attr) = getimagesize("images/" . $recette['image']);
            if($height > $height_max)
            {
                $width = ($height_max / $height) * $width;
                $height = $height_max;
            }
            $tailles_images[$recette['id']]['width'] = $width;
            $tailles_images[$recette['id']]['height'] = $height;
        }
        return view('recettes.index', compact('recettes', 'titre', 'tailles_images', 'height_max'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $titre = $this->titre;
        return view('recettes.create', compact('titre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
        ]);
        $nom = time().'.'.$request->image->getClientOriginalExtension();
		$request->image->move(public_path('images'), $nom);
        $image_id = Image::create(['nom' => $nom])->id;
        $recette = Recette::create(['nom' => $request->nom, 'description' => $request->description, 'temps' => $request->temps, 'image' => $image_id, 'personnes' => $request->personnes]);
        return redirect(route('recette.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recette  $recette
     * @return \Illuminate\Http\Response
     */
    public function show(Recette $recette)
    {
        $height_max = $this->taille_max;
        $tailles_image = [];
        list($width, $height, $type, $attr) = getimagesize("images/" . $recette['image']);
        if($height > $height_max)
        {
            $width = ($height_max / $height) * $width;
            $height = $height_max;
        }
        $tailles_image['width'] = $width;
        $tailles_image['height'] = $height;

        $titre = $this->titre;
        $etapes = Recette_etape::etapeAscending()->where('recette', $recette['id'])->get();
        $recette_ingredient_unites = DB::table('recette_ingredient_unites')
            ->join('ingredients', 'ingredients.id', '=', 'recette_ingredient_unites.ingredient')
            ->join('unites', 'unites.id', '=', 'recette_ingredient_unites.unite')
            ->select('recette_ingredient_unites.*', 'ingredients.nom as nom_ingredient', 'unites.nom as nom_unite', 'unites.abreviation as abr_unite')
            ->where('recette', $recette['id'])
            ->get();
        return view('recettes.show', compact('recette', 'titre', 'etapes', 'recette_ingredient_unites', 'tailles_image'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recette  $recette
     * @return \Illuminate\Http\Response
     */
    public function edit(Recette $recette)
    {
        $titre = $this->titre;

        $height_max = $this->taille_max;
        $tailles_image = [];
        list($width, $height, $type, $attr) = getimagesize("images/" . $recette['image']);
        if($height > $height_max)
        {
            $width = ($height_max / $height) * $width;
            $height = $height_max;
        }
        $tailles_image['width'] = $width;
        $tailles_image['height'] = $height;

        $etapes = Recette_etape::etapeAscending()->where('recette', $recette['id'])->get();
        $recette_ingredient_unites = DB::table('recette_ingredient_unites')
            ->join('ingredients', 'ingredients.id', '=', 'recette_ingredient_unites.ingredient')
            ->join('unites', 'unites.id', '=', 'recette_ingredient_unites.unite')
            ->select('recette_ingredient_unites.*', 'ingredients.nom as nom_ingredient', 'unites.nom as nom_unite', 'unites.abreviation as abr_unite')
            ->where('recette', $recette['id'])
            ->get();
        return view('recettes.edit', compact('recette', 'titre', 'etapes', 'recette_ingredient_unites', 'tailles_image'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recette  $recette
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recette $recette)
    {
        if($request->image != null)
        {
            $request->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:8192',
            ]);
            $nom = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $nom);
            $image_id = Image::create(['nom' => $nom])->id;
            $recette->update(['nom' => $request->nom, 'description' => $request->description, 'temps' => $request->temps, 'image' => $image_id, 'personnes' => $request->personnes]);
        }
        else
            $recette->update(['nom' => $request->nom, 'description' => $request->description, 'temps' => $request->temps, 'personnes' => $request->personnes]);
        return redirect(route('recette.edit', $recette));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recette  $recette
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recette $recette)
    {
        $recette->delete();
        return redirect(route('recette.index'));
    }
    
    public function recherche(Request $request)
    {
        $titre = $this->titre;
        $height_max = 225;
        $recherche = $request->recherche;
        $recettes = Recette::where('nom', 'LIKE', '%' . $recherche . '%')->idDescending()->get();
        $tailles_images = [];
        foreach($recettes as $recette)
        {
            list($width, $height, $type, $attr) = getimagesize("images/" . $recette['image']);
            if($height > $height_max)
            {
                $width = ($height_max / $height) * $width;
                $height = $height_max;
            }
            $tailles_images[$recette['id']]['width'] = $width;
            $tailles_images[$recette['id']]['height'] = $height;
        }
        return view('recettes.index', compact('recettes', 'titre', 'tailles_images', 'height_max', 'recherche'));
    }
}
