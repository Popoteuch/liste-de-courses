<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Image;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUpload()
    {
        return view('imageUpload');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function imageUploadPost(Request $request)
    {
        // dd($request->image);
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $nom = time().'.'.$request->image->getClientOriginalExtension();
		$request->image->move(public_path('images'), $nom);
		$image = Image::create(['nom' => $nom])->id;
		dd($image);
        return back()
            ->with('success','You have successfully upload image.')
            ->with('image', $nom);
    }
}
