<?php

namespace App\Http\Controllers;

use App\Recette_ingredient_unite;
use App\Recette;
use App\Ingredient;
use App\Unite;
use Illuminate\Http\Request;
use DB;

class RecetteIngredientUniteController extends Controller
{
    protected $titre = "Ingrédients";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($recette)
    {
        $req_ingredients = Ingredient::select(['nom', 'id'])->orderBy('ingredients.nom','ASC')->get();
        $ingredients = [];
        foreach($req_ingredients as $req_ingredient)
        {
            $ingredients[$req_ingredient['id']] = $req_ingredient['nom'];
        }
		$req_unites = Unite::select(['nom', 'id'])->orderBy('unites.nom', 'ASC')->get();
        $unites = [];
        foreach($req_unites as $req_unite)
        {
            $unites[$req_unite['id']] = $req_unite['nom'];
        }
        $titre = $this->titre;
        return view('recettes.ingredients.create', compact('recette', 'titre', 'ingredients', 'unites'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$recette = Recette::find($request->recette);
        Recette_ingredient_unite::create(['recette' => $request->recette, 'ingredient' => $request->ingredient, 'quantite' => $request->quantite, 'unite' => $request->unite]);
        return redirect(route('recette.edit', $recette));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Recette_ingredient_unite $recette_ingredient_unite)
    {
        $titre = $this->titre;
        
        $req_ingredients = Ingredient::select(['nom', 'id'])->orderBy('ingredients.nom','ASC')->get();
        $ingredients = [];
        foreach($req_ingredients as $req_ingredient)
        {
            $ingredients[$req_ingredient['id']] = $req_ingredient['nom'];
        }
        $req_unites = Unite::select(['nom', 'id'])->orderBy('unites.nom','ASC')->get();
        $unites = [];
        foreach($req_unites as $req_unite)
        {
            $unites[$req_unite['id']] = $req_unite['nom'];
        }
        return view('recettes.ingredients.edit', compact('recette_ingredient_unite', 'ingredients', 'unites', 'titre'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recette_ingredient_unite $recette_ingredient_unite)
    {
        $recette = Recette::find($recette_ingredient_unite['recette']);
        Recette_ingredient_unite::where('id', $recette_ingredient_unite['id'])
                                ->update(['quantite' => $request['quantite'], 'ingredient' => $request['ingredient'], 'unite' => $request['unite']]);
        return redirect(route('recette.edit', $recette));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recette_ingredient_unite $recette_ingredient_unite)
    {
        $recette = Recette::find($recette_ingredient_unite['recette']);
        $recette_ingredient_unite->delete();
        return redirect(route('recette.edit', $recette));
    }
}
