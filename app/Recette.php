<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Image;

class Recette extends Model
{
    protected $fillable = ['nom', 'description', 'temps', 'image', 'personnes'];

    public function getImageAttribute($value)
    {
        $image = Image::find($value);
        return $image['nom'];
    }

    public function scopeIdDescending($query)
    {
        return $query->orderBy('id','DESC');
    }
}
