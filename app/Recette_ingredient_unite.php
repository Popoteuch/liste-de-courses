<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recette_ingredient_unite extends Model
{
    protected $fillable = ['quantite', 'recette', 'ingredient', 'unite'];
}
